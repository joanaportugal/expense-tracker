import React from "react";
import { Provider } from "react-redux";

import BalanceTotal from "./components/BalanceTotal";
import TransactionForm from "./components/TransactionForm";
import History from "./components/History";

import store from "./store";

import "./App.css";

function App() {
  return (
    <Provider store={store}>
      <h1>MyBank</h1>
      <BalanceTotal />
      <TransactionForm />
      <History />
    </Provider>
  );
}

export default App;
