import React from "react";
import { connect } from "react-redux";

import "./style.css";

const BalanceTotal = ({ state }) => {
  return (
    <>
      <section className="balance">
        <h3>Your Balance</h3>
        <h3>{state.total} €</h3>
      </section>

      <section className="totals">
        <article>
          <span>Income</span>
          <span style={{ color: "darkgreen" }}>{state.incomeTotal} €</span>
        </article>
        <article>
          <span>Expense</span>
          <span style={{ color: "red" }}>{state.expenseTotal} €</span>
        </article>
      </section>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    state: state,
  };
};

export default connect(mapStateToProps)(BalanceTotal);
