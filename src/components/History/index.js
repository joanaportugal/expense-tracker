import React from "react";
import { connect } from "react-redux";

import "./style.css";

const History = ({ history }) => {
  const bgColor = (type) => (type === "Income" ? "lightgreen" : "red");

  const letterColor = (type) => (type === "Income" ? "black" : "white");

  return (
    <>
      <section className="history">
        <h2>History</h2>
        {history.map((transaction) => (
          <article
            style={{
              background: bgColor(transaction.type),
            }}
          >
            <span
              style={{
                color: letterColor(transaction.type),
              }}
            >
              <b>{transaction.description}</b>
            </span>
            <span
              style={{
                color: letterColor(transaction.type),
              }}
            >
              {transaction.amount} €
            </span>
          </article>
        ))}
      </section>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    history: state.history,
  };
};

export default connect(mapStateToProps)(History);
