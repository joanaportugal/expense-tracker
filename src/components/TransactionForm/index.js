import React, { useState } from "react";
import { connect } from "react-redux";

import { addExpenseAction, addIncomeAction } from "../../store/actions";

import "./style.css";

const TransactionForm = ({ total, addIncome, addExpense }) => {
  const [description, setDescription] = useState("");
  const [type, setType] = useState("Income");
  const [amount, setAmount] = useState(0);

  const verification = !description || amount === 0;

  const addTransaction = (e) => {
    e.preventDefault();
    if (type === "Income") {
      addIncome(description, Number(amount));
    }
    if (type === "Expense") {
      if (total - Number(amount) > 0) addExpense(description, Number(amount));
      else alert("You can't spend more than what you have");
    }
    setDescription("");
    setAmount(0);
  };

  return (
    <section className="addTransaction">
      <h2>Add new transaction</h2>
      <form>
        <label>
          <span>Description</span>
          <input
            type="text"
            placeholder="Enter description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
        </label>
        <label>
          <span>Type</span>
          <select value={type} onChange={(e) => setType(e.target.value)}>
            <option value="Expense">Expense</option>
            <option value="Income">Income</option>
          </select>
        </label>
        <label>
          <span>Amount</span>
          <input
            type="number"
            placeholder="Enter amount"
            value={amount}
            onChange={(e) => setAmount(e.target.value)}
            min={0}
          />
        </label>
        <button disabled={verification} onClick={(e) => addTransaction(e)}>
          Add Transaction
        </button>
      </form>
    </section>
  );
};

const mapStateToProps = (state) => {
  return {
    total: state.total,
  };
};

const mapDispatchToProps = (dispatch) => ({
  addExpense: (description, value) =>
    dispatch(addExpenseAction(description, value)),
  addIncome: (description, value) =>
    dispatch(addIncomeAction(description, value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TransactionForm);
