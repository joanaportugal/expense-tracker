import { ADD_EXPENSE, ADD_INCOME } from "./constants";

export const addExpenseAction = (description, value) => ({
  type: ADD_EXPENSE,
  description,
  value,
});

export const addIncomeAction = (description, value) => ({
  type: ADD_INCOME,
  description,
  value,
});
