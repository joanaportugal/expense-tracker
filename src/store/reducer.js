import { ADD_EXPENSE, ADD_INCOME } from "./constants";

const initialState = {
  total: 0,
  incomeTotal: 0,
  expenseTotal: 0,
  history: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_EXPENSE:
      return {
        ...state,
        total: state.total - action.value,
        expenseTotal: state.expenseTotal + action.value,
        history: [
          ...state.history,
          {
            description: action.description,
            type: "Expense",
            amount: action.value,
          },
        ],
      };
    case ADD_INCOME:
      return {
        ...state,
        total: state.total + action.value,
        incomeTotal: state.incomeTotal + action.value,
        history: [
          ...state.history,
          {
            description: action.description,
            type: "Income",
            amount: action.value,
          },
        ],
      };
    default:
      return state;
  }
};

export default reducer;
